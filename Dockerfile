FROM alpine:3.18
LABEL Author="XCompWiz <xcompwiz@gmail.com>"

# This hack is widely applied to avoid python printing issues in docker containers.
# See: https://github.com/aws/amazon-sagemaker-examples/issues/319
ENV PYTHONUNBUFFERED=1

RUN echo "**** install Python & wget ****" && \
    apk add --no-cache python3 wget && \
    if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python ; fi && \
    \
    echo "**** install pip ****" && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --no-cache --upgrade pip setuptools wheel requests feedparser praw && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi
