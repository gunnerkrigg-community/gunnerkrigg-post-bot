# Introduction
This is a self-contained environment for running a frequent check for the latest Gunnerkrigg court comic.
When a comic is posted, this service detects that posting and automatically publishes the link to it on Reddit.

The service is written in Python.

# Docker Image
The environment for this service is built from this same repo.

The image is based on Alpine Linux image, which is only a 5MB image.
The Dockerfile adds the latest version of Python3 as well as a few necessary packages for this project.

We then build the image and put it into this project's container registry, so that the scheduled CI job can use it.

# Local Testing
To execute this locally, you will need to have Docker installed (and running).
If you use Docker, you do not need to install Python to your own machine.

- `export DOCKER_IMAGE_BUILD_TAG="gunnerkrigg-python:latest"`
- `docker build --pull -t $DOCKER_IMAGE_BUILD_TAG .`
- `docker run -it --rm -v $(pwd):/mnt/workdir $DOCKER_IMAGE_BUILD_TAG`

This will create a local container of the environment for the project, in which you can execute
- `cd /mnt/workdir/`
- `python src/main.py`

## Windows Bash
If running from Git Bash on Windows, the Docker run command should be adjusted to account for the terminal.
- `winpty docker run -it --rm -v /$(pwd):/mnt/workdir $DOCKER_IMAGE_BUILD_TAG`
