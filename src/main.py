import feedparser
import os
import praw

APP_VERSION='0.1.0'

REDDIT_CLIENT_ID = os.getenv('REDDIT_CLIENT_ID')
REDDIT_CLIENT_SECRET = os.getenv('REDDIT_CLIENT_SECRET')
REDDIT_API_USER = os.getenv('REDDIT_API_USER')
REDDIT_API_PASSWORD = os.getenv('REDDIT_API_PASSWORD')

def parse_rss(feed_url):
    parsed_feed = feedparser.parse(feed_url)
    return parsed_feed.entries

def compare_feeds(old_links, new_feed_entries):
    changes = []
    for entry in new_feed_entries:
        if entry.link not in old_links:
            changes.append(entry)
    return changes

def compare(old_feed_file, new_feed_url):
    old_feed_entries = parse_rss(old_feed_file)
    new_feed_entries = parse_rss(new_feed_url)
    old_links = [entry.link for entry in old_feed_entries]
    return compare_feeds(old_links, new_feed_entries)

new_entries = compare('./rss-previous.xml', './rss-current.xml')

if new_entries:
    print("New Posts Detected:")
    if REDDIT_CLIENT_ID and REDDIT_CLIENT_SECRET and REDDIT_API_USER and REDDIT_API_PASSWORD:
        reddit = praw.Reddit(
            user_agent = f'linux:gunnerkrigg-post-bot:v{APP_VERSION} (by u/xcompwiz)',
            client_id = REDDIT_CLIENT_ID,
            client_secret = REDDIT_CLIENT_SECRET,
            username = REDDIT_API_USER,
            password = REDDIT_API_PASSWORD,
        )
        for entry in new_entries:
            shortdesc = entry.description.split('-----', 1)[0]
            reddit.subreddit('gunnerkrigg').submit(title=entry.title, url=entry.link)
            print("Posted to reddit:", entry.title)
    else:
        for entry in new_entries:
            print("- Title:", entry.title)
            print("  Link:", entry.link)
            print("  Description:", entry.description.split('-----', 1)[0])
            print("  Published:", entry.published)
            print()
else:
    print("No new posts detected.")
